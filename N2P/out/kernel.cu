#include "zhang.h"

#include <stdio.h>
#include <cstdlib>
#include <iostream>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "math.h"

#include "N2P.h"

#define CHK_ERROR if (erro != cudaSuccess) goto Error;



__device__ 
int contaVizinhos(unsigned char matriz[3][3]) {
	
	return (P0 + P1 + P2 + P3 + P4 + P5 + P6 + P7)/PIXEL;
}
__device__ 
int contaTransicoes (unsigned char matriz[3][3]) {

	unsigned char sequencia[] = {P2, P3, P4, P5, P6, P7, P0, P1, P2};   

	int numeroTransicao = 0;
	for(int i = 1; i < 10; i++) {
		if (sequencia[i-1] == APAGADO && sequencia[i] == PIXEL)
			numeroTransicao++;
	}

	return numeroTransicao;
}
__device__ 
unsigned char ZhangSuen(unsigned char matriz[3][3], int passo) {

	if (matriz[1][1] == APAGADO)
		return APAGADO;

	int nVizinhos = contaVizinhos(matriz);
	if(nVizinhos < 2 || nVizinhos > 6)
		return PIXEL;

	int nTransicoes = contaTransicoes(matriz);
	if(nTransicoes != 1)
		return PIXEL;

	int vizinhaca;
	
	if(passo == 1){
		vizinhaca = P2 * P4 * P6;
		if(vizinhaca != APAGADO)
			return PIXEL;

		return P4 * P6 * P0 == APAGADO ? APAGADO : PIXEL;
	}
	//Passo 2
	vizinhaca = P2 * P4 * P0;
	if(vizinhaca != APAGADO)
		return PIXEL;

	return P2 * P6 * P0 == APAGADO ? APAGADO : PIXEL;
}
__device__ 
bool verificaDiferenca(unsigned char* image_data, unsigned char* nova_imagem, int largura, int altura) {
	
	for(int linha = 1; linha < altura-1; linha++) {
		for(int coluna = 1; coluna < largura-1; coluna++) {
			
			int index = linha*largura + coluna;
			if(image_data[index] != nova_imagem[index])
				return true;
		}
	}
	return false;
}


__global__
void N2P_kernel(unsigned char* imagem, unsigned char* imageResult, int largura, int altura, data param){

	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int linha = index/largura;
	int coluna = index%largura;
	
	unsigned char matrix[3][3];
		
	for(int a = -1; a < 2; a++)
		for(int b = -1; b < 2; b++)
			matrix[a+1][b+1] = imagem[largura * (linha+a) + coluna+b];
	
	imageResult[index] = ZhangSuen(matrix, linha, coluna);	   		
}

extern "C" void kN2P(unsigned char* imagem, unsigned char* imageResult, int width, int height){

	cudaDeviceProp deviceProp;
	cudaError_t erro;	
	unsigned char* d_image;
	unsigned char* d_imageResult;

	long tamanho = width*height;
		
	erro = cudaMalloc((void**)&d_image,sizeof(unsigned char)*tamanho); CHK_ERROR
	erro = cudaMalloc((void**)&d_imageResult,sizeof(unsigned char)*tamanho); CHK_ERROR
	
	

	erro = cudaMemcpy(d_image,imagem,tamanho*sizeof(unsigned char),cudaMemcpyHostToDevice);

	
	
	cudaGetDeviceProperties(&deviceProp, 0);

	int blockSize = deviceProp.maxThreadsPerBlock;
	int nBlocks = tamanho/blockSize + (tamanho%blockSize == 0 ? 0:1);

	dim3 dim = (width,height); 

	N2P_kernel<<<nBlocks, blockSize>>>(d_image, d_imageResult, width, height);

	erro = cudaGetLastError(); CHK_ERROR
	
	//Espera por GPU terminar o trabalho
	erro = cudaDeviceSynchronize(); CHK_ERROR
	
	erro = cudaMemcpy(imageResult,d_imageResult,tamanho*sizeof(unsigned char),cudaMemcpyDeviceToHost); CHK_ERROR

Error:	
	std::cerr << "Error on CUDA: " << cudaGetErrorString(erro);
	cudaFree(d_image);
	cudaFree(d_imageResult);

}
