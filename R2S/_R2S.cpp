#include <opencv2//highgui//highgui.hpp>
#include <iostream>

#include "R2S.h"

template <class T>
R2S<T>::R2S(IplImage* imagemInput) : imagemOriginal((unsigned char*) imagemInput->imageData) {
	altura = imagemInput->height;
	largura = imagemInput->widthStep;
	tamanho = altura * largura;
}

template <class T>
R2S<T>::R2S(unsigned char* imagemInput, int altura, int largura, #STRUCT parametros): 
	imagemOriginal(imagemInput), 
	altura(altura), largura(largura), tamanho(altura*largura), parametros(parametros)
	{	}

template <class T>
bool R2S<T>::verificaHardwareCompativel(cudaError_t* error_id){

		int deviceCount;
	
		*error_id = cudaGetDeviceCount(&deviceCount);
	
		if(*error_id != cudaSuccess || deviceCount < 1)		
			return false;
	
		return true;
	}

template <class T>
T R2S<T>::processa() {
	std::cout << "Type unsupported" << std::endl;
	return 0;
}

template <class T>
int R2S<T>::base_processa() {

	if(!verificaHardwareCompativel(&erro))
		return erro;

	return cudaSetDevice(0);
}

template <>
int R2S<int>::processa() {
	
	int erro = base_processa();
	if(erro != cudaSuccess)
		return erro;

	return kR2S_int(imagemOriginal, tamanho, parametros);
}

template <>
float R2S<float>::processa() {
	
	int erro = base_processa();
	if(erro != cudaSuccess)
		return erro;

	return kR2S_float(imagemOriginal, tamanho, parametros);
}

template <>
double R2S<double>::processa() {
	
	int erro = base_processa();
	if(erro != cudaSuccess)
		return erro;

	return kR2S_double(imagemOriginal, tamanho, parametros);
}

template <>
unsigned char R2S<unsigned char>::processa() {
	
	int erro = base_processa();
	if(erro != cudaSuccess)
		return erro;

	return kR2S_uchar(imagemOriginal, tamanho, parametros);
}
