﻿#include <opencv2//core//core.hpp>
#include <opencv2//highgui//highgui.hpp>
#include <cuda_runtime.h>

#include "N2P.h"

class N2P {
	
	IplImage* imagemOriginal;
	int tamanho;
	int altura;
	int largura;

	data parametros;
	cudaError_t erro;

private:

	bool verificaHardwareCompativel(cudaError_t* error_id){

		int deviceCount;
	
		*error_id = cudaGetDeviceCount(&deviceCount);
	
		if(*error_id != cudaSuccess || deviceCount < 1)		
			return false;
	
		return true;
	}

	//Converte o vetor imagem para um IplImage e apresenta os resultados
	void mostraResultado(IplImage* imagemOriginal, unsigned char* imagemProcessada){
	
		IplImage *imagemFinal;
		imagemFinal = cvCreateImageHeader(cvSize(imagemOriginal->width,imagemOriginal->height), IPL_DEPTH_8U, 1);
		cvSetData(imagemFinal,imagemProcessada,imagemOriginal->widthStep);

		cvShowImage("Original", imagemOriginal);
		cvShowImage("Polarizada", imagemFinal);
		
		cvWaitKey(100000);
	}

public:
	
	N2P(IplImage* imagemInput) : imagemOriginal(imagemInput) {
		altura = imagemInput->height;
		largura = imagemInput->width;
		data nada = {};
		parametros = nada;
	}

	N2P(IplImage* imagemInput, int tamanho, data parametros): imagemOriginal(imagemInput), tamanho(tamanho), parametros(parametros) {	}

	int processaN2P (){		

		if(!verificaHardwareCompativel(&erro)){
			return erro;
		}

		erro = cudaSetDevice(0);
		if (erro != cudaSuccess) {
			return erro;
		}

		unsigned char* imageData = (unsigned char*) imagemOriginal->imageData;
		unsigned char* imagemResultante;
	
		imagemResultante = (unsigned char*)malloc(sizeof(unsigned char)*imagemOriginal->imageSize);

		N2P_kernel(imageData, imagemResultante, largura,altura, parametros);

		mostraResultado(imagemOriginal,imagemResultante);

		return erro == cudaSuccess;		
	}

	const char* getError() const {
		return cudaGetErrorString(erro);
	}
};

int main() {
	IplImage *imagem = cvLoadImage("img.jpg", CV_LOAD_IMAGE_GRAYSCALE);
	N2P n2p(imagem);	
	n2p.processaN2P();
}
