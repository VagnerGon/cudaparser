Regras da API P2P

Deve-se criar uma funcao de acordo com a seguinte estrutura:

/*	
	Deve ser precedido pelo marcador #P2P_FUNC
	Retorno deve ser do tipo unsigned char; (representando o pixel)
	Deve receber um unsigned char que é o pixel da imagem de entrada
	Pode receber uma struct (definida pelo marcador #STRUCT) como parametro
	E receber um inteiro que é o indice do pixel da imagem de entrada	
*/
#P2P_FUNC
unsigned char nomeFuncao(unsigned char pixel, param parametro, int indice);

Para executar, importe o arquivo P2P.h e chame a função 

/*Retorna a imagem processada*/ 
unsigned char* P2P(unsigned char* imagemInput, int tamanho, param parametros);