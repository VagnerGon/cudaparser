#ifndef __R2V_HEADER__

	#define  __R2V_HEADER__
	
	#include <cuda_runtime.h>
	#include <opencv2//core//core.hpp>

	#STRUCT_DECLARATION
	
	extern "C" void kR2V(unsigned char* imagem, int* d_Histogram, int tamanho, int nbins, #STRUCT parametros);

	class R2V {

		protected:

			static bool verificaHardwareCompativel(cudaError_t* error_id);
			cudaError_t erro;

		public:	
		
			#STRUCT parametros;

			int tamanho;
			int altura;
			int largura;
			int nBins;
			unsigned char* imagemOriginal;
			int* result;

			explicit R2V(IplImage* imagemInput, int nBins) ;

			R2V(unsigned char* imagemInput, int altura, int largura, int nBins, #STRUCT parametros);

			int processa();
	};

#endif