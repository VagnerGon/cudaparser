#include "Core.h"

#include <opencv2//highgui//highgui.hpp>

Core::Core(IplImage* imagemInput) : imagemOriginal((unsigned char*) imagemInput->imageData) {
	altura = imagemInput->height;
	largura = imagemInput->width;
	tamanho = altura * largura;
}

Core::Core(unsigned char* imagemInput, int altura, int largura): 
	imagemOriginal(imagemInput), 
	altura(altura), largura(largura), tamanho(altura*largura)
	{	}

bool Core::verificaHardwareCompativel(cudaError_t* error_id){

	int deviceCount;
	
	*error_id = cudaGetDeviceCount(&deviceCount);
	
	if(*error_id != cudaSuccess || deviceCount < 1)		
		return false;
	
	return true;
}

//Converte o vetor imagem para um IplImage e apresenta os resultados
	
void Core::mostraResultado(IplImage* imagemOriginal, unsigned char* imagemProcessada){
	
	IplImage *imagemFinal;
	imagemFinal = cvCreateImageHeader(cvSize(imagemOriginal->width,imagemOriginal->height), IPL_DEPTH_8U, 1);
	cvSetData(imagemFinal,imagemProcessada,imagemOriginal->widthStep);

	cvShowImage("Original", imagemOriginal);
	cvShowImage("Polarizada", imagemFinal);

	cv::imwrite("result.jpg", cv::Mat(imagemFinal));
		
	cvWaitKey(100000);
}
	
void Core::mostraResultado(IplImage* imagemOriginal) const {
	mostraResultado(imagemOriginal, imagemResultante);
}

const char* Core::getError() const {
	return cudaGetErrorString(erro);
}

int Core::processa() {
		
	if(!verificaHardwareCompativel(&erro))
		return erro;

	erro = cudaSetDevice(0);

	if (erro != cudaSuccess)
		return erro;		
	
	imagemResultante = (unsigned char*)malloc(tamanho);

	return processaEspecifico(imagemOriginal, imagemResultante, largura,altura);
}

