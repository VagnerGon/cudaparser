#include <cuda_runtime.h>
#include "device_launch_parameters.h"
#include "P2P.h"
#include <stdio.h>

typedef struct {
	char* imageSubtration;
	int limiar;
	int indice;
}data;

__device__ 
unsigned char subtraction(char in, data param) {

	int result = in-param.imageSubtration[param.indice] - param.limiar;
	result = result > 0 ? result : result * -1;
	return result;
}

__global__ void P2P_kernel(unsigned char* imagem, unsigned char* imageResult, int tamanho, data parametros){
    int i = blockIdx.x * blockDim.x + threadIdx.x;
	    if(i < tamanho)
        	imageResult[i] = subtraction (imagem[i], parametros);
}

extern "C" void P2P(unsigned char* imagem, unsigned char* imageResult, int tamanho, data parametros){

	cudaDeviceProp deviceProp;
	cudaError_t erro;
	unsigned char* d_image;
	unsigned char* d_imageResult;

	erro = cudaMalloc((void**)&d_image,sizeof(unsigned char)*tamanho);
	if (erro != cudaSuccess) {
		goto Error;
	}
	erro = cudaMalloc((void**)&d_imageResult,sizeof(unsigned char)*tamanho);
	if (erro != cudaSuccess) {
		goto Error;
	}

	erro = cudaMemcpy(d_image,imagem,tamanho*sizeof(unsigned char),cudaMemcpyHostToDevice);

	cudaGetDeviceProperties(&deviceProp, 0);
	int blockSize = deviceProp.maxThreadsPerBlock;
	int nBlocks = tamanho/blockSize + (tamanho%blockSize == 0 ? 0:1);

	P2P_kernel<<<nBlocks, blockSize>>>(d_image, d_imageResult,tamanho, parametros);

	erro = cudaGetLastError();
	if (erro != cudaSuccess) {
		goto Error;
	}
	//Espera por GPU terminar o trabalho
	erro = cudaDeviceSynchronize();
	if (erro != cudaSuccess) {
		goto Error;
	}
	erro = cudaMemcpy(imageResult,d_imageResult,tamanho*sizeof(unsigned char),cudaMemcpyDeviceToHost);
	if (erro != cudaSuccess) {
		goto Error;
	}

	Error:
    cudaFree(d_image);
    cudaFree(d_imageResult);
    
}
