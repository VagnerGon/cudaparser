#include <cuda.h>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "R2S.h"
#include <iostream>

#define CHK_ERROR if (erro != cudaSuccess) goto Error;				

//Retorna zero caso forem iguais

__device__

unsigned char media(unsigned char pixel1, unsigned char pixel2) {
	
	return (pixel1+pixel2)/2;
}


__global__ void R2S_kernel(unsigned char* imagem, unsigned char* result, int tamanho) {
	
	unsigned int index = threadIdx.x + blockIdx.x * blockDim.x * 2;

	if(index > tamanho)
		return;		

	__shared__ unsigned char smem[1024];//TODO usar blocksize
	unsigned int sIndex = threadIdx.x;

	if(index+blockDim.x < tamanho)
		smem[sIndex] = media(imagem[index], imagem[index+blockDim.x]);
	else
		smem[sIndex] = imagem[index];

	__syncthreads();

	for(unsigned int cmp_pixel = blockDim.x/2; cmp_pixel > 0; cmp_pixel=cmp_pixel/2) {
		if(sIndex < cmp_pixel && index+sIndex+cmp_pixel < tamanho)
			smem[sIndex] = media(smem[sIndex],smem[sIndex+cmp_pixel]);
		__syncthreads();
	}

	if(threadIdx.x == 0)
		result[blockIdx.x] = smem[0];
}

extern "C" long kR2S(unsigned char* imagem, int tamanho) {
	
	cudaDeviceProp deviceProp;
	cudaError_t erro;	
	unsigned char* d_image;
	unsigned char* d_result;
	bool singleBlock = false;

	erro = cudaMalloc((void**)&d_image,sizeof(unsigned char)*tamanho); CHK_ERROR

	erro = cudaMemcpy(d_image,imagem,tamanho*sizeof(unsigned char),cudaMemcpyHostToDevice);	
	
	cudaGetDeviceProperties(&deviceProp, 0);

	int blockSize = deviceProp.maxThreadsPerBlock;
	int nBlocks = tamanho/blockSize + (tamanho%blockSize == 0 ? 0:1);
	
	if(nBlocks == 1) {
		nBlocks++;
		singleBlock = true;
	}

	nBlocks = nBlocks % 2 == 1 ? nBlocks/2+1 : nBlocks/2;

	erro = cudaMalloc((void**)&d_result,sizeof(unsigned char)*nBlocks); CHK_ERROR

	R2S_kernel<<<nBlocks, blockSize>>>(d_image, d_result,tamanho);

	erro = cudaGetLastError(); CHK_ERROR
	
	//Espera por GPU terminar o trabalho
	erro = cudaDeviceSynchronize(); CHK_ERROR
	
	unsigned char* result = (unsigned char*) malloc(sizeof(unsigned char)*nBlocks);
	erro = cudaMemcpy(result, d_result, sizeof(unsigned char)*nBlocks,cudaMemcpyDeviceToHost); CHK_ERROR

	while (!singleBlock) {
		
		cudaFree(d_image);		
		cudaFree(d_result);

		int dimensaoNova = nBlocks;

		erro = cudaMalloc((void**)&d_image,sizeof(unsigned char)*dimensaoNova); CHK_ERROR

		erro = cudaMemcpy(d_image,result,dimensaoNova*sizeof(unsigned char),cudaMemcpyHostToDevice);	

		nBlocks = dimensaoNova/blockSize + (dimensaoNova%blockSize == 0 ? 0:1);
		
		if(nBlocks == 1) {
			nBlocks++;
			singleBlock = true;
		}
		
		nBlocks = nBlocks % 2 == 1 ? nBlocks/2+1 : nBlocks/2;

		erro = cudaMalloc((void**)&d_result,sizeof(unsigned char)*nBlocks); CHK_ERROR

		R2S_kernel<<<nBlocks, blockSize>>>(d_image, d_result,dimensaoNova);

		erro = cudaGetLastError(); CHK_ERROR
	
		//Espera por GPU terminar o trabalho
		erro = cudaDeviceSynchronize(); CHK_ERROR		

		erro = cudaMemcpy(result, d_result, sizeof(unsigned char)*nBlocks,cudaMemcpyDeviceToHost); CHK_ERROR
	} 
		
	cudaFree(d_image);		
	cudaFree(d_result);

	return result[0];
	
Error:	
	std::cerr << "Error on CUDA: " << cudaGetErrorString(erro);
	cudaFree(d_image);
	cudaFree(d_result);
	return -1;
}
 
