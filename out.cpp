#include <opencv2//core//core.hpp>
#include <opencv2//highgui//highgui.hpp>

#P2P_FUNC
static unsigned char polarizador(unsigned char pixel){
	return pixel > 127 ? 255 {
	return pixel > 127 ? 255 : 0;
: 0;
}

//Converte o vetor imagem para um IplImage e apresenta os resultados
void mostraResultado(IplImage* imagemOriginal, unsigned char* imagemProcessada){

	IplImage *imagemFinal;
	imagemFinal = cvCreateImageHeader(cvSize(imagemOriginal->width,imagemOriginal->height), IPL_DEPTH_8U, 1);
	cvSetData(imagemFinal,imagemProcessada,imagemOriginal->widthStep);

	cvShowImage("Original", imagemOriginal);
	cvShowImage("Polarizada", imagemFinal);

	cvWaitKey(100000);
}

int main(){

	IplImage *imagem = cvLoadImage("img.jpg", CV_LOAD_IMAGE_GRAYSCALE);
	auto imagebytes = imagem->imageData;
	int tamanhoImagem = imagem->imageSize;

	unsigned char* imagemResultante;
	imagemResultante = static_cast<unsigned char*>(malloc(sizeof(unsigned char)*imagem->imageSize));

	for(int i=0; i<tamanhoImagem;i++)
	{
		imagemResultante[i] = polarizador(imagebytes[i]);
	}

	mostraResultado(imagem,imagemResultante);

}

