#include <cuda_runtime.h>
#include "device_launch_parameters.h"
#include "P2P.h"
#include <stdio.h>

#define CHK_ERROR if (erro != cudaSuccess) goto Error;

__device__ data param;

__device__ 
static unsigned char subtraction(char in, data param, int i) {

	int result = in-param.imageSubtration[i] - param.limiar;
	result = result > 0 ? result : result * -1;
	return result;
}


__global__ void P2P_kernel(unsigned char* imagem, unsigned char* imageResult, int tamanho, data param){
    int i = blockIdx.x * blockDim.x + threadIdx.x;
	    if(i < tamanho)
        	imageResult[i] = result = result > 0 ? result : result * -1;
	return result;
}

#STRUCT
typedef struct {
	char* imageSubtration;
	int limiar;
} data;

void mostraResultado (imagem[i], param, i);
}

extern "C" void kP2P(unsigned char* imagem, unsigned char* imageResult, int tamanho, data param){

	cudaDeviceProp deviceProp;
	cudaError_t erro;
	unsigned char* d_image;
	unsigned char* d_imageResult;

	erro = cudaMalloc((void**)&d_image,sizeof(unsigned char)*tamanho); CHK_ERROR
	erro = cudaMalloc((void**)&d_imageResult,sizeof(unsigned char)*tamanho); CHK_ERROR
	
	erro = cudaMalloc((void**)&param.imageSubtration,sizeof(parametros.imageSubtration)); CHK_ERROR

	erro = cudaMemcpy(d_image,imagem,tamanho*sizeof(unsigned char),cudaMemcpyHostToDevice);
	erro = cudaMemcpy(param.imageSubtration,parametros.imageSubtration,sizeof(parametros.imageSubtration),cudaMemcpyHostToDevice); CHK_ERROR
    //erro = cudaMemcpy(param.imageSubtration,parametros.imageSubtration,sizeof(parametros.imageSubtration),cudaMemcpyHostToDevice);

	cudaGetDeviceProperties(&deviceProp, 0);
	int blockSize = deviceProp.maxThreadsPerBlock;
	int nBlocks = tamanho/blockSize + (tamanho%blockSize == 0 ? 0:1);

	P2P_kernel<<<nBlocks, blockSize>>>(d_image, d_imageResult,tamanho, param);

	erro = cudaGetLastError(); CHK_ERROR
	
	//Espera por GPU terminar o trabalho
	erro = cudaDeviceSynchronize(); CHK_ERROR
	
	erro = cudaMemcpy(imageResult,d_imageResult,tamanho*sizeof(unsigned char),cudaMemcpyDeviceToHost); CHK_ERROR
	
	Error:
    cudaFree(d_image);
    cudaFree(d_imageResult);   
}
