#include <opencv2//core//core.hpp>
#include <opencv2//highgui//highgui.hpp>
#include <cuda_runtime.h>
#include <iostream>

#include "R2S.h"

bool verificaHardwareCompativel(cudaError_t* error_id){

		int deviceCount;
	
		*error_id = cudaGetDeviceCount(&deviceCount);
	
		if(*error_id != cudaSuccess || deviceCount < 1)		
			return false;
	
		return true;
	}

cudaError_t processaR2S (IplImage* imagem){

	cudaError_t erro;

	if(!verificaHardwareCompativel(&erro)){
		return erro;
	}

	erro = cudaSetDevice(0);
	if (erro != cudaSuccess) {
		return erro;
	}

	unsigned char* imageData = (unsigned char*) imagem->imageData;

	std::cout << "Resultado: " << kR2S(imageData, imagem->imageSize);

	return erro;
}

int main(){

	IplImage *imagem = cvLoadImage("in.jpg", CV_LOAD_IMAGE_GRAYSCALE);
	processaR2S(imagem);	
}
