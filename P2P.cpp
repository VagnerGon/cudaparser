#include <opencv2//core//core.hpp>
#include <opencv2//highgui//highgui.hpp>
#include <cuda_runtime.h>
#include "P2P.h"

class P2P{
	
	IplImage* imagemOriginal;
	int tamanho;

	data parametros;
	cudaError_t erro;

private:

	bool verificaHardwareCompativel(cudaError_t* error_id){

		int deviceCount;
	
		*error_id = cudaGetDeviceCount(&deviceCount);
	
		if(*error_id != cudaSuccess || deviceCount < 1)		
			return false;
	
		return true;
	}

	//Converte o vetor imagem para um IplImage e apresenta os resultados
	void mostraResultado(IplImage* imagemOriginal, unsigned char* imagemProcessada){
	
		IplImage *imagemFinal;
		imagemFinal = cvCreateImageHeader(cvSize(imagemOriginal->width,imagemOriginal->height), IPL_DEPTH_8U, 1);
		cvSetData(imagemFinal,imagemProcessada,imagemOriginal->widthStep);

		cvShowImage("Original", imagemOriginal);
		cvShowImage("Polarizada", imagemFinal);
		
		cvWaitKey(100000);
	}

public:
	
	P2P(IplImage* imagemInput, int tamanho) : imagemOriginal(imagemInput), tamanho(tamanho){	}

	P2P(IplImage* imagemInput, int tamanho, data parametros): imagemOriginal(imagemInput), tamanho(tamanho), parametros(parametros) {	}

	int processaP2P (){		

		if(!verificaHardwareCompativel(&erro)){
			return erro;
		}

		erro = cudaSetDevice(0);
		if (erro != cudaSuccess) {
			return erro;
		}

		unsigned char* imageData = (unsigned char*) imagemOriginal->imageData;
		unsigned char* imagemResultante;
	
		imagemResultante = (unsigned char*)malloc(sizeof(unsigned char)*imagemOriginal->imageSize);

		kP2P(imageData, imagemResultante, tamanho, parametros);

		return erro == cudaSuccess;		
	}

	const char* getError() const {
		return cudaGetErrorString(erro);
	}
};
