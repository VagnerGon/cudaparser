﻿#ifndef __N2P_HEADER__

	#define  __N2P_HEADER__

	#include <cuda_runtime.h>
	#include "Core.h"
	#include <opencv2//core//core.hpp>

	#USER_INCLUDE

	#STRUCT_DECLARATION

	extern "C" void kN2P(unsigned char* imagem, unsigned char* imageResult, int width, int height, #STRUCT parametros);

	class N2P : public Core{		
	
	public:

		#STRUCT parametros;

		N2P(IplImage* imagemInput);

		N2P(unsigned char* imagemInput, int altura, int largura, #STRUCT parametros);
	
		int processaEspecifico(unsigned char* imagemOriginal, unsigned char* imagemResultante, int largura, int altura) override;
	};

#endif
