#include <opencv2//core//core.hpp>
#include <opencv2//highgui//highgui.hpp>

#include <ctime>

#include "R2V.h"
#include <iostream>


R2V::R2V(IplImage* imagemInput, int nBins) : imagemOriginal((unsigned char*) imagemInput->imageData), nBins(nBins) {
	altura = imagemInput->height;
	largura = imagemInput->widthStep;
	tamanho = altura * largura;
}

R2V::R2V(unsigned char* imagemInput, int altura, int largura, int nBins, #STRUCT parametros): 
	imagemOriginal(imagemInput), 
	altura(altura), largura(largura), tamanho(altura*largura), 
	nBins(nBins), parametros(parametros)
	{	}

bool R2V::verificaHardwareCompativel(cudaError_t* error_id){

	int deviceCount;
	
	*error_id = cudaGetDeviceCount(&deviceCount);
	
	if(*error_id != cudaSuccess || deviceCount < 1)		
		return false;
	
	return true;
}

int R2V::processa() {
		
	if(!verificaHardwareCompativel(&erro))
		return erro;

	erro = cudaSetDevice(0);

	if (erro != cudaSuccess)
		return erro;		
		
	result = (int*)malloc(sizeof(int)*256);
	
	kR2V((unsigned char*) imagemOriginal, result, tamanho, 256, parametros);

	return erro == cudaSuccess ? 0 : erro;	
}

static void printaTempo(clock_t* start, char* texto) {
	clock_t finish = clock() - *start;
	double interval = finish / (double)CLOCKS_PER_SEC; 
	std::cout << texto <<  std::endl << " - " << interval * 1000 << "ms" << std::endl;
	*start = clock();
}

int main(int argc, char* argv[]){

	clock_t start,finish;

	start = clock();

	const char* imagemEntrada = argc == 1 ? "img.jpg" : argv[1]; 

	IplImage *imagem = cvLoadImage(imagemEntrada, CV_LOAD_IMAGE_GRAYSCALE);
	
	printaTempo(&start, "Carregada imagem");

	R2V r2v = R2V(imagem, 256);

	r2v.processa();

	finish = clock() - start;
	double interval = finish / (double)CLOCKS_PER_SEC; 

	for(int i=0; i<256;i++) {
		std::cout << r2v.result[i] << " ";
	}

	//std::cout << "Resultado: " << +maior << std::endl;
	std::cout << std::endl << "tempo : " << interval * 1000 << "ms" << std::endl;

	return 0;
}
