#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <iostream>

#include <stdio.h>

#define UINT_BITS 32
#define LOG2_WARP_SIZE 5U
#define WARP_SIZE (1U << LOG2_WARP_SIZE)

#define MERGE_THREADBLOCK_SIZE 256

//Threadblock size
#define HISTOGRAM256_THREADBLOCK_SIZE (WARP_COUNT * WARP_SIZE)

//Warps ==subhistograms per threadblock
#define WARP_COUNT 6

#define CHK_ERROR if (erro != cudaSuccess) goto Error;

#R2V_FUNC

__global__ void R2V_kernel(unsigned char* imagem, int* result, int nbins, int tamanho, #STRUCT param) {

	//Per-warp subhistogram storage
    extern __shared__ int s_Hist[];
    int *s_WarpHist= s_Hist + (threadIdx.x >> LOG2_WARP_SIZE) * nbins;

    //Clear shared memory storage for current threadblock before processing

    for (int i = 0; i < (WARP_COUNT * nbins / HISTOGRAM256_THREADBLOCK_SIZE); i++)
    {
        s_Hist[threadIdx.x + i * HISTOGRAM256_THREADBLOCK_SIZE] = 0;
    }

    //Cycle through the entire data set, update subhistograms for each warp
    const int tag = threadIdx.x << (UINT_BITS - LOG2_WARP_SIZE);

    __syncthreads();

	//Faz acesso nao coescalado para evitar conflitos de memoria compartilhada
	for (int pos = blockIdx.x * blockDim.x + threadIdx.x; pos < tamanho; pos += blockDim.x * gridDim.x) {        
        #R2V_CALL(imagem[pos], s_WarpHist, nbins, param);
    }

    __syncthreads();

	//Faz uma merge semando as respctivas posicos de cada subhistograma dos warps para o histograma do bloco, gravado na memoria global.
	for (int bin = threadIdx.x; bin < nbins; bin += HISTOGRAM256_THREADBLOCK_SIZE)
    {
        int sum = 0;

        for (int i = 0; i < WARP_COUNT; i++)
        {
            sum += s_Hist[bin + i * nbins]; //& TAG_MASK
        }

        result[blockIdx.x * nbins + bin] = sum;
    }
}

__global__ void mergeHistogram256Kernel(int *d_Histogram, int *d_PartialHistograms, int histogramCount, int nbins) {
    
	int sum = 0;

    for (int i = threadIdx.x; i < histogramCount; i += MERGE_THREADBLOCK_SIZE)
    {
        sum += d_PartialHistograms[blockIdx.x + i * nbins];
    }

    __shared__ int data[MERGE_THREADBLOCK_SIZE];
    data[threadIdx.x] = sum;

    for (int stride = MERGE_THREADBLOCK_SIZE / 2; stride > 0; stride >>= 1)
    {
        __syncthreads();

        if (threadIdx.x < stride)
        {
            data[threadIdx.x] += data[threadIdx.x + stride];
        }
    }

    if (threadIdx.x == 0)
    {
        d_Histogram[blockIdx.x] = data[0];
    }
}

static int* d_PartialHistograms;
//histogram256kernel() intermediate results buffer
static const int PARTIAL_HISTOGRAM256_COUNT = 240;

extern "C" void kR2V(unsigned char* imagem, int* histogram, int tamanho, int nbins, #STRUCT param) {
	
	unsigned char* d_image;
	int* d_Histogram;

	erro = cudaMalloc((void**)&d_image,sizeof(unsigned char)*tamanho); CHK_ERROR

	erro =cudaMalloc((void **)&d_PartialHistograms, PARTIAL_HISTOGRAM256_COUNT * nbins * sizeof(int)); CHK_ERROR
	
	erro =cudaMalloc((void **)&d_Histogram, nbins * sizeof(int)); CHK_ERROR
	
	erro = cudaMemcpy(d_image,imagem,tamanho*sizeof(unsigned char),cudaMemcpyHostToDevice);

	//Passa tamanho de shared memory a ser alocada
	R2V_kernel<<<PARTIAL_HISTOGRAM256_COUNT, HISTOGRAM256_THREADBLOCK_SIZE, WARP_COUNT * nbins * sizeof(int)>>>
		(d_image, (int*) d_PartialHistograms, nbins, tamanho / sizeof(unsigned char));

	erro = cudaGetLastError(); CHK_ERROR
	//erro = cudaDeviceSynchronize(); CHK_ERROR

	mergeHistogram256Kernel<<<nbins, nbins>>>(d_Histogram, d_PartialHistograms, PARTIAL_HISTOGRAM256_COUNT, nbins);

	erro = cudaGetLastError(); CHK_ERROR	
	erro = cudaDeviceSynchronize(); CHK_ERROR

	erro = cudaMemcpy(histogram,d_Histogram,nbins*sizeof(int),cudaMemcpyDeviceToHost); CHK_ERROR

	goto Free;
Error:	
	std::cerr << "Error on CUDA: " << cudaGetErrorString(erro);

Free:
	cudaFree(d_image);
	cudaFree(d_PartialHistograms);
	cudaFree(d_Histogram);
}