package control;

import java.io.File;

/**
 * Created by Vagmer on 01/03/2016.
 */
public class TipoProcessador {

    public TIPO_PROCESSAMENTO tipo;

    public String FUNC_TAG;
    public String FUNC_CALL;
    public String RETURN_TYPE_REGEX;

    public String directory;
    public String userCode;
    public String mainCpp;
    public String header;
    public String kernel;

    public String mainCppOut;
    public String headerOut;
    public String kernelOut;

    public TipoProcessador(TIPO_PROCESSAMENTO tipo_processamento){

        tipo = tipo_processamento;

        this.FUNC_TAG = tipo_processamento.FUNC_TAG;
        this.FUNC_CALL = tipo_processamento.FUNC_CALL;
        this.RETURN_TYPE_REGEX = tipo_processamento.RETURN_TYPE_REGEX;
        this.directory = tipo_processamento.dir;

        this.userCode = tipo_processamento.userCode;
        this.mainCpp = directory + File.separator + tipo_processamento.mainCpp;
        this.header = directory + File.separator + tipo_processamento.header;
        this.kernel = directory + File.separator + tipo_processamento.kernel;
        this.mainCppOut = directory + File.separator + tipo_processamento.mainCppOut;
        this.headerOut = directory + File.separator + tipo_processamento.headerOut;
        this.kernelOut = directory + File.separator + tipo_processamento.kernelOut;
    }

    public TipoProcessador(String FUNC_TAG, String FUNC_CALL, String userCode, String mainCpp, String header, String kernel, String mainCppOut, String headerOut, String kernelOut, String RETURN_TYPE_REGEX) {
        this.FUNC_TAG = FUNC_TAG;
        this.FUNC_CALL = FUNC_CALL;
        this.RETURN_TYPE_REGEX = RETURN_TYPE_REGEX;
        this.userCode = userCode;
        this.mainCpp = mainCpp;
        this.header = header;
        this.kernel = kernel;
        this.mainCppOut = mainCppOut;
        this.headerOut = headerOut;
        this.kernelOut = kernelOut;
    }
}
