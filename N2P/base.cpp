#include <stdio.h>

#include <opencv2//core//core.hpp>
#include <opencv2//highgui//highgui.hpp>
#include <opencv2//imgproc//imgproc.hpp>

#STRUCT
typedef struct {
    int nada;
} data;

#P2P_FUNC
unsigned char sobel(unsigned char matrix[3][3], int i, int j, data param) {
	float kernelx[3][3] = {{-1, 0, 1},
						   {-2, 0, 2},
						   {-1, 0, 1}};

	float kernely[3][3] = {{-1, -2, -1},
						   {0,  0,  0},
						   {1,  2,  1}};
	double magX = 0.0;
	double magY = 0.0;

	for(int a = 0; a < 3; a++){
		for(int b = 0; b < 3; b++){
			magX += matrix[a][b] * kernelx[a][b];
			magY += matrix[a][b] * kernely[a][b];
		}
	}
	return sqrt(magX*magX + magY*magY);
}

int main(int argc, char ** argv){
	IplImage *imageRaw, *imageResult;
	unsigned char* image, *result;
	imageRaw = cvLoadImage("img.jpg", CV_LOAD_IMAGE_GRAYSCALE);
	image = (unsigned char*)imageRaw->imageData;	

	cvShowImage("Original", imageRaw);

	int tamanho = imageRaw->width*imageRaw->height;

	printf("nSize: %d - imageSize: %d\n", imageRaw->nSize, sizeof(unsigned char)*imageRaw->imageSize);

	result = (unsigned char*) malloc(sizeof(unsigned char)*imageRaw->imageSize);

	for(int z = imageRaw->width; z < tamanho-imageRaw->width; z++){

		result[z] = somaMagnitude(imageRaw, image, z);
	}

	imageResult = cvCreateImageHeader(cvSize(imageRaw->width,imageRaw->height), IPL_DEPTH_8U, 1);
	cvSetData(imageResult,result,imageRaw->widthStep);

	cvShowImage("Original", imageRaw);
	cvShowImage("Sobel", imageResult);
		
	cvWaitKey(100000);

	return processador(soma, 0);
}