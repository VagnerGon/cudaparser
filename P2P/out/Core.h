#ifndef __CORE_HEADER__

	#define  __CORE_HEADER__

	#include <cuda_runtime.h>

	#include <opencv2//core//core.hpp>


	class Core{
	
		int tamanho;
		int altura;
		int largura;

		protected:

			static bool verificaHardwareCompativel(cudaError_t* error_id);

			cudaError_t erro;

		public:
	
			unsigned char* imagemOriginal;
			unsigned char* imagemResultante;

			Core(IplImage* imagemInput);

			Core(unsigned char* imagemInput, int altura, int largura);		

			static void mostraResultado(IplImage* imagemOriginal, unsigned char* imagemProcessada);
	
			void mostraResultado(IplImage* imagemOriginal) const;

			int processa();

			virtual int processaEspecifico(unsigned char* imagemOriginal, unsigned char* imagemResultante, int largura, int altura) = 0;

			const char* getError() const;
	};

#endif
