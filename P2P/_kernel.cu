#include <cuda_runtime.h>
#include <iostream>
#include "device_launch_parameters.h"
#include "P2P.h"

#define CHK_ERROR if (erro != cudaSuccess) goto Error;

#P2P_FUNC

__global__ void P2P_kernel(unsigned char* imagem, unsigned char* imageResult, int tamanho, #STRUCT param){
    int i = blockIdx.x * blockDim.x + threadIdx.x;
	    if(i < tamanho)
        	imageResult[i] = #P2P_CALL (imagem[i], i, param);
}

extern "C" void kP2P(unsigned char* imagem, unsigned char* imageResult, int tamanho, #STRUCT param){

	cudaDeviceProp deviceProp;
	cudaError_t erro;
	unsigned char* d_image;
	unsigned char* d_imageResult;

	erro = cudaMalloc((void**)&d_image,sizeof(unsigned char)*tamanho); CHK_ERROR
	erro = cudaMalloc((void**)&d_imageResult,sizeof(unsigned char)*tamanho); CHK_ERROR
	
	erro = cudaMemcpy(d_image,imagem,tamanho*sizeof(unsigned char),cudaMemcpyHostToDevice);
	
	cudaGetDeviceProperties(&deviceProp, 0);
	int blockSize = deviceProp.maxThreadsPerBlock;
	int nBlocks = tamanho/blockSize + (tamanho%blockSize == 0 ? 0:1);

	P2P_kernel<<<nBlocks, blockSize>>>(d_image, d_imageResult,tamanho, param);

	erro = cudaGetLastError(); CHK_ERROR
	
	//Espera por GPU terminar o trabalho
	erro = cudaDeviceSynchronize(); CHK_ERROR
	
	erro = cudaMemcpy(imageResult,d_imageResult,tamanho*sizeof(unsigned char),cudaMemcpyDeviceToHost); CHK_ERROR
	
	goto Free;
Error:	
	std::cerr << "Error on CUDA: " << cudaGetErrorString(erro);
	
Free:
	cudaFree(d_image);
	cudaFree(d_imageResult);
}
