#include <iostream>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "N2P.h"

#define CHK_ERROR if (erro != cudaSuccess) goto Error;

#N2P_FUNC

__global__
void N2P_kernel(unsigned char* imagem, unsigned char* imageResult, int largura, int altura, #STRUCT param){

	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int linha = index/largura;
	int coluna = index%largura;
	
	unsigned char matrix[3][3];
		
	for(int a = -1; a < 2; a++)
		for(int b = -1; b < 2; b++)
			matrix[a+1][b+1] = imagem[largura * (linha+a) + coluna+b];
	
	imageResult[index] = #N2P_CALL(matrix, linha, coluna, param);	   		
}

extern "C" void kN2P(unsigned char* imagem, unsigned char* imageResult, int width, int height, #STRUCT param){

	cudaDeviceProp deviceProp;
	cudaError_t erro;	
	unsigned char* d_image;
	unsigned char* d_imageResult;

	long tamanho = width*height;
		
	erro = cudaMalloc((void**)&d_image,sizeof(unsigned char)*tamanho); CHK_ERROR
	erro = cudaMalloc((void**)&d_imageResult,sizeof(unsigned char)*tamanho); CHK_ERROR
	
	erro = cudaMemcpy(d_image,imagem,tamanho*sizeof(unsigned char),cudaMemcpyHostToDevice);

	cudaGetDeviceProperties(&deviceProp, 0);

	int blockSize = deviceProp.maxThreadsPerBlock;
	int nBlocks = tamanho/blockSize + (tamanho%blockSize == 0 ? 0:1);

	dim3 dim = (width,height); 

	N2P_kernel<<<nBlocks, blockSize>>>(d_image, d_imageResult, width, height, param);

	erro = cudaGetLastError(); CHK_ERROR
	
	//Espera por GPU terminar o trabalho
	erro = cudaDeviceSynchronize(); CHK_ERROR
	
	erro = cudaMemcpy(imageResult,d_imageResult,tamanho*sizeof(unsigned char),cudaMemcpyDeviceToHost); CHK_ERROR

	goto Free;
Error:	
	std::cerr << "Error on CUDA: " << cudaGetErrorString(erro);
	
Free:
	cudaFree(d_image);
	cudaFree(d_imageResult);

}