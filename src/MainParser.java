import control.TIPO_PROCESSAMENTO;
import control.TipoProcessador;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Vagmer on 21/11/2015.
 */
public class MainParser {

    protected String FUNC_TAG = "#FUNC_TAG";
    protected String FUNC_CALL = "#FUNC_CALL";
    protected String USER_INCLUDES = "#USER_INCLUDE";
    protected String RETURN_TYPE_REGEX = "( |\\n)*static( |\\n)*unsigned( |\\n)*char ";

    protected static final String STRUCT = "#STRUCT";
    protected static final String STRUCT_DECLARATION = "#STRUCT_DECLARATION";
    protected static final String STRUCT_ALOCATE = "#ALOCATE_STRUCT_POINTER";
    protected static final String STRUCT_MEM_CPY = "#MEM_CPY_STRUCT_POINTERS";

    protected String structName;
    private List<String> ponteiroStruct;

    private void replaceTag(String tag, StringBuilder source, CharSequence replacement, int index){

        source.replace(index, index + tag.length(),"");
        source.insert(index, replacement);
    }
    private int replaceTag(String tag, StringBuilder source, CharSequence replacement){

        int index = source.indexOf(tag);
        if(index >= 0)
            replaceTag(tag, source, replacement, index);
        return index;
    }

    private void replaceTags(String tag, StringBuilder source, CharSequence replacement) {

        while (replaceTag(tag, source, replacement) > 0);
    }

    //Substitui apenas a chamada do metodo dentro do kernel
    //Caso haja mais de um metodo __device__, procura pelo MAIN
    private void putKernelCall(StringBuilder codeInserted, StringBuilder baseKernelCode) throws Exception {

        int indexFuncTag;

        indexFuncTag = codeInserted.indexOf(FUNC_TAG+"_MAIN");
        if(indexFuncTag > 0) {
            String oldValue = FUNC_TAG;
            FUNC_TAG = FUNC_TAG+"_MAIN";
            putFuncCall(indexFuncTag,codeInserted,baseKernelCode);
            replaceTag(FUNC_TAG,codeInserted,oldValue,indexFuncTag);
            FUNC_TAG = oldValue;
        }else {
            indexFuncTag = codeInserted.indexOf(FUNC_TAG);
            putFuncCall(indexFuncTag,codeInserted,baseKernelCode);
        }
    }

    private CharSequence insertCodeBlock(StringBuilder code, StringBuilder baseKernelCode){

        int indexFuncDeclaration = code.indexOf(FUNC_TAG);  //Localiza TAG no codigo do usuario

        //Remove TAG do codigo do usuario
        replaceTag(FUNC_TAG, code,"",indexFuncDeclaration);

        //Obtem limitadores do bloco da funcao
        int blockBeginning = code.indexOf("{",indexFuncDeclaration);
        int blockEnding = findBlockEnding(code, blockBeginning+1);

        CharSequence blockP2P = "\n__device__ " + code.subSequence(indexFuncDeclaration, blockEnding+1); //Obtem bloco de codigo da funcao
        int indexKernelLocation = baseKernelCode.indexOf(FUNC_TAG); //Mantem TAG no lugar para caso de futuras insercoes
        baseKernelCode.insert(indexKernelLocation -1, blockP2P); //Insere bloco no kernel antes da TAG.

        //Repete operações com outras chamadas da função remanascentes
        if(code.indexOf(FUNC_TAG) > 0)
            return insertCodeBlock(code, baseKernelCode);
        else    //Nao ha mais funcoes a adicionar, entao remove tag
            replaceTag(FUNC_TAG, baseKernelCode,"");

        return code;
    }

    private void putFuncCall(int indexFuncDeclaration, StringBuilder code, StringBuilder baseKernelCode) throws Exception {

        Pattern pattern = Pattern.compile(FUNC_TAG+ RETURN_TYPE_REGEX);
        Matcher matcher = pattern.matcher(code.subSequence(indexFuncDeclaration, code.length()));

        if(!matcher.find())
            throw new Exception("No function found!");

        //int prefix = (FUNC_TAG + "\nstatic unsigned char ").length();
        String codeCall = getCodeLimited(code, indexFuncDeclaration+ matcher.end(), "(", false);

        while (replaceTag(FUNC_CALL, baseKernelCode, codeCall) > 0) //Replace em todas as referencias
            putFuncCall(indexFuncDeclaration, code, baseKernelCode);
    }

    private String getCodeLimited(StringBuilder code, int offset, String limit, boolean getLast) {

        char[] block = new char[1000];
        int posFinal = code.indexOf(limit, offset);
        posFinal += (getLast ? 1 : 0);
        code.getChars(offset, posFinal,block,0);
        return new String(block,0,posFinal -offset);
    }

    public MainParser(TipoProcessador metodo, String auxiliarHeader) throws Exception {

        BufferedReader kernelReader;
        BufferedReader mainReader;
        BufferedReader cppReader;
        BufferedReader headerReader;

        mainReader = new BufferedReader(new FileReader(metodo.userCode));
        cppReader = new BufferedReader(new FileReader(metodo.mainCpp));
        headerReader = new BufferedReader(new FileReader(metodo.header));
        kernelReader = new BufferedReader(new FileReader(metodo.kernel));

        this.FUNC_TAG = metodo.FUNC_TAG;
        this.FUNC_CALL = metodo.FUNC_CALL;
        this.RETURN_TYPE_REGEX = metodo.RETURN_TYPE_REGEX;

        StringBuilder baseKernelCode = loadFile(kernelReader);
        StringBuilder mainCode = loadFile(mainReader);
        StringBuilder headerCode = loadFile(headerReader);
        StringBuilder cppCode = loadFile(cppReader);

        insertAuxiliarHeader(baseKernelCode, auxiliarHeader);
        //Insere funcao principal
        putKernelCall(mainCode, baseKernelCode);
        insertCodeBlock(mainCode, baseKernelCode);

        insertHeaders(mainCode, headerCode);

        //Procura declaracao da struct de dados e insere codigo no header
        if(mainCode.indexOf(STRUCT) > 0) {
            insereDeclaracaoStructHeader(headerCode, getStruct(mainCode, baseKernelCode));

            alocateStructPointerIntoKernel(baseKernelCode);
            copyMemoryFromStructPointers(baseKernelCode);

            //Insere as structs nos respectivo arquivos
            insereStuctUses(headerCode, structName);
            insereStuctUses(cppCode, structName);
            insereStuctUses(baseKernelCode, structName);
        }else
            removeStuctThings(baseKernelCode, cppCode, headerCode);

        if(metodo.tipo == TIPO_PROCESSAMENTO.N2P || metodo.tipo == TIPO_PROCESSAMENTO.P2P )
            CopyCoreFiles(metodo.directory);

        writeResultInFile(baseKernelCode, metodo.kernelOut);
        writeResultInFile(headerCode, metodo.headerOut);
        writeResultInFile(cppCode, metodo.mainCppOut);
    }

    private void CopyCoreFiles(String directory) {

        File coreCpp = new File("Core" + File.separator + "Core.cpp");
        File coreHpp = new File("Core" + File.separator + "Core.h");

        File cppOutputCore = new File(directory+File.separator+"out"+File.separator+"Core.cpp");
        File hppOutputCore = new File(directory+File.separator+"out"+File.separator+"Core.h");

        if(!cppOutputCore.exists())
            cppOutputCore.getParentFile().mkdirs();

        try {
            Files.copy(coreCpp.toPath(), cppOutputCore.toPath(), StandardCopyOption.REPLACE_EXISTING);
            Files.copy(coreHpp.toPath(), hppOutputCore.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }catch (Exception ex){
            System.err.println("Impossible to copy core files");
        }
    }

    private void insertAuxiliarHeader(StringBuilder baseKernelCode, String auxiliarHeader) {

        if(!auxiliarHeader.isEmpty())
            baseKernelCode.insert(0, "#include \""+ auxiliarHeader +"\"\n\n");
    }

    private void removeStuctThings(StringBuilder baseKernelCode, StringBuilder mainCode, StringBuilder headerCode) {

        replaceTags(STRUCT_DECLARATION, headerCode,"");  //Remove declaracao
        replaceTags(", " + STRUCT + " parametros", headerCode,""); //Remove usos do objeto
        replaceTags(STRUCT + " parametros;", headerCode,""); //Remove objeto da classe

        replaceTags(", " + STRUCT + " parametros", mainCode,""); //Remove parametro do header
        replaceTags(", parametros(parametros)", mainCode,""); //Remove parametro do construtor
        replaceTags(", parametros", mainCode,""); //Remove parametro da chamada ao kernel

        replaceTags("__device__ " + STRUCT + " d_param;", baseKernelCode,""); //Remove parametro do header
        replaceTags(", " + STRUCT + " param", baseKernelCode,""); //Remove parametro dos headeres
        replaceTags(", d_param", baseKernelCode,""); //Remove parametro da chamada ao kernel
        replaceTags(", param", baseKernelCode,""); //Remove parametro da chamada a funcao dentro do kernel
        replaceTags(STRUCT_ALOCATE, baseKernelCode,"");
        replaceTags(STRUCT_MEM_CPY, baseKernelCode,"");
    }

    private void insereDeclaracaoStructHeader(StringBuilder headerCode, String structCode) {

        replaceTag(STRUCT_DECLARATION, headerCode, structCode);
    }

    private void insertHeaders(StringBuilder mainCode, StringBuilder headerCode) {

        int offset = 0;
        int index;

        int place = replaceTag(USER_INCLUDES,headerCode,"");

        while ((index = mainCode.indexOf("#include", offset)) >= 0){

            offset = mainCode.indexOf("\n", index);
            if(offset < 0)
                offset = mainCode.indexOf("\r", index);

            String includeLine = mainCode.substring(index,offset);
            headerCode.insert(place,includeLine+"\n");
            place += offset-index+1;
        }
    }

    private void structPointerOperation(StringBuilder baseKernelCode, String base, String TAG){

        int index = baseKernelCode.indexOf(TAG);
        if(index < 0)
            return;

        replaceTag(TAG,baseKernelCode,"");

        if (ponteiroStruct.size() == 0)
            return;

        String newPoiter = "";
        for (String ponteiro : ponteiroStruct) {
            newPoiter += base.replace("#", ponteiro);
            newPoiter += " CHK_ERROR";
        }
        baseKernelCode.insert(index, newPoiter);
    }

    private void copyMemoryFromStructPointers(StringBuilder baseKernelCode) {

        structPointerOperation(baseKernelCode,
                "erro = cudaMemcpy(d_param.#,param.#,sizeof(param.#),cudaMemcpyHostToDevice);",
                STRUCT_MEM_CPY);

    }

    private void alocateStructPointerIntoKernel(StringBuilder baseKernelCode) {

        structPointerOperation(baseKernelCode,
                "erro = cudaMalloc((void**)&d_param.#,sizeof(param.#));",
                STRUCT_ALOCATE);
    }

    private String getStruct(StringBuilder mainCode, StringBuilder kernelCode) throws Exception {

        int indexStruct = mainCode.indexOf(STRUCT);
        String structCode;
        int declaration = mainCode.indexOf("typedef struct", indexStruct);

        if(declaration < 0)
            declaration = mainCode.indexOf("struct", indexStruct);

        if (declaration < 0)
            throw new Exception("No struct after #STRUCT!");

        structCode = getCodeLimited(mainCode,declaration,"}", true);

        getStuctPoiters(structCode);

        structName = getCodeLimited(mainCode,declaration+structCode.length()+1,";",false); //Concatena nome
        structCode = structCode + structName + ";";

        insereStuctUses(kernelCode, structName);

        return structCode;
    }

    //Retona lista com cada elemento que eh ponteiro da struct, para futura realocacao
    private void getStuctPoiters(String structCode) {

        //String[] valores = structCode.split(System.getProperty("line.separator"));

        ponteiroStruct = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new StringReader(structCode));
        try {
            String valor;

            while ((valor = reader.readLine()) != null){
                if (valor.indexOf("*") > 0)
                    ponteiroStruct.add(valor.split(" ")[1].replace("*","").replace(";","")); //Obtem apenas o nome, sem o ponteiro, se houver
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void insereStuctUses(StringBuilder kernelCode, String structName) {

        int index;
        while((index = kernelCode.indexOf(STRUCT+" ")) > 0){
            replaceTag(STRUCT, kernelCode, structName,index);
        }
    }

    private void writeResultInFile(CharSequence codeWithFuncP2P, String saida) {

        FileOutputStream out;
        try {
            File fSaida = new File(saida);
            if(!fSaida.exists()) {
                fSaida.getParentFile().mkdirs();
                fSaida.createNewFile();
            }
            out = new FileOutputStream(saida);
            PrintStream prt = new PrintStream(out);
            prt.print(codeWithFuncP2P.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int findBlockEnding(StringBuilder sFile, int blockBeginning) {

        int nextBrace = sFile.indexOf("{", blockBeginning);
        int nextEndingBrace = sFile.indexOf("}", blockBeginning);

        if(nextBrace != -1 && (nextBrace < nextEndingBrace)) {
            nextEndingBrace = findBlockEnding(sFile, nextBrace+1);
            return findBlockEnding(sFile, nextEndingBrace+1);
        }

        return nextEndingBrace;
    }

    protected StringBuilder loadFile(BufferedReader kernelReader) {

        String linha;
        StringBuilder code = new StringBuilder();
        try {
            while ((linha = kernelReader.readLine()) != null) {
                code.append(linha);
                code.append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return code;
    }

    public static void main(String[] args) {

        if( args.length < 2) {
            System.out.println("Not enough parameters! Provide Kind (P2P, N2P, R2S or R2V) and source code.");
            return;
        }

        TIPO_PROCESSAMENTO tipo;

        try {
            switch (args[0]){
                case "P2P":
                    tipo = TIPO_PROCESSAMENTO.P2P;
                    break;
                case "N2P":
                    tipo = TIPO_PROCESSAMENTO.N2P;
                    break;
                case "R2S":
                    tipo = TIPO_PROCESSAMENTO.R2S;
                    break;
                case "R2V":
                    tipo = TIPO_PROCESSAMENTO.R2V;
                    break;
                default:
                    System.out.println("Incorrect Kind. Must be (P2P, N2P, R2S or R2V)");
                    return;
            }
            tipo.setFile(args[1]);

            TipoProcessador tipoProcessador = new TipoProcessador(tipo);

            new MainParser(tipoProcessador,  args.length > 2 ? args[2] : "");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
