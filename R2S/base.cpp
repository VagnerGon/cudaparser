#include <stdio.h>

#include <opencv2//core//core.hpp>
#include <opencv2//highgui//highgui.hpp>
#include <opencv2//imgproc//imgproc.hpp>

#R2S_FUNC
unsigned char media(unsigned char pixel1, unsigned char pixel2) {
	
	return (pixel1+pixel2)/2;
}

int main(int argc, char ** argv){
	IplImage *imageRaw, *imageResult;
	unsigned char* image, *result;
	imageRaw = cvLoadImage("img.jpg", CV_LOAD_IMAGE_GRAYSCALE);
	image = (unsigned char*)imageRaw->imageData;	

	cvShowImage("Original", imageRaw);

	int tamanho = imageRaw->width*imageRaw->height;

	printf("nSize: %d - imageSize: %d\n", imageRaw->nSize, sizeof(unsigned char)*imageRaw->imageSize);

	result = (unsigned char*) malloc(sizeof(unsigned char)*imageRaw->imageSize);

	for(int z = imageRaw->width; z < tamanho-imageRaw->width; z++){

		result[z] = somaMagnitude(imageRaw, image, z);
	}

	imageResult = cvCreateImageHeader(cvSize(imageRaw->width,imageRaw->height), IPL_DEPTH_8U, 1);
	cvSetData(imageResult,result,imageRaw->widthStep);

	cvShowImage("Original", imageRaw);
	cvShowImage("Sobel", imageResult);
		
	cvWaitKey(100000);

	return processador(soma, 0);
}