#include "Subtraction.h"
#include <opencv2//core//core.hpp>
#include <opencv2//highgui//highgui.hpp>
#ifndef __P2P_HEADER__

	#define  __P2P_HEADER__

	#include "Core.h"
	#include <opencv2//core//core.hpp>
	#include <cuda_runtime.h>	

	typedef struct {
	char* imageSubtration;
	int limiar;
}data;

	extern "C" void kP2P(unsigned char* imagem, unsigned char* imageResult, int tamanho, data parametros);
	
	class P2P : public Core{
	
		public:	

		data parametros;

		P2P(IplImage* imagemInput);

		P2P(unsigned char* imagemInput, int altura, int largura, data parametros);

		int processaEspecifico(unsigned char* imagemOriginal, unsigned char* imagemResultante, int largura, int altura) override;
	};

#endif
