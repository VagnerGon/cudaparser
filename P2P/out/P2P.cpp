#include "P2P.h"

#include <opencv2//highgui//highgui.hpp>
#include <ctime>
#include <iostream>

P2P::P2P(IplImage* imagemInput) : Core((unsigned char*) imagemInput->imageData, imagemInput->height, imagemInput->widthStep) {  }

P2P::P2P(unsigned char* imagemInput, int altura, int largura, data parametros): Core(imagemInput, altura, largura), parametros(parametros) {	}

int P2P::processaEspecifico(unsigned char* imagemOriginal, unsigned char* imagemResultante, int largura, int altura){		

	
	kP2P(imagemOriginal, imagemResultante, tamanho, parametros);
	
	return erro == cudaSuccess;
}
