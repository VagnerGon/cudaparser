﻿#include "N2P.h"

N2P::N2P(IplImage* imagemInput) : Core((unsigned char*) imagemInput->imageData, imagemInput->height, imagemInput->widthStep) { }

N2P::N2P(unsigned char* imagemInput, int altura, int largura, #STRUCT parametros): Core(imagemInput, altura, largura), parametros(parametros) {	}

int N2P::processaEspecifico(unsigned char* imagemOriginal, unsigned char* imagemResultante, int largura, int altura){		
	
	kN2P(imagemOriginal, imagemResultante, largura,altura, parametros);
				
	return erro == cudaSuccess;		
}
