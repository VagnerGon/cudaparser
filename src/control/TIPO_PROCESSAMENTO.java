package control;

/**
 * Created by Vagmer on 01/03/2016.
 */
public enum TIPO_PROCESSAMENTO {

    P2P("#P2P_FUNC","#P2P_CALL","P2P","base.cpp","_P2P.cpp","_P2P.h","_kernel.cu","out/P2P.cpp","out/P2P.h","out/kernel.cu",  "( |\\n)*static( |\\n)*unsigned( |\\n)*char "),
    N2P("#N2P_FUNC","#N2P_CALL","N2P","zhang.cpp","_N2P.cpp","_N2P.h","_kernel.cu","out/N2P.cpp","out/N2P.h","out/kernel.cu", "( |\\n)*static( |\\n)*unsigned( |\\n)*char "),
    R2S("#R2S_FUNC","#R2S_CALL","R2S","base.cpp","_R2S.cpp","_R2S.h","_kernel.cu","out/R2S.cpp","out/R2S.h","out/kernel.cu",  "( |\\n)*static( |\\n)*T( |\\n)* "),
    R2V("#R2V_FUNC","#R2V_CALL","R2V","base.cpp","_R2V.cpp","_R2V.h","_kernel.cu","out/R2V.cpp","out/R2V.h","out/kernel.cu",  "( |\\n)*static( |\\n)*void( |\\n)* ");

    String RETURN_TYPE_REGEX;
    String FUNC_TAG;
    String FUNC_CALL;

    String dir;

    String userCode;
    String mainCpp;
    String header;
    String kernel;

    String mainCppOut;
    String headerOut;
    String kernelOut;

    TIPO_PROCESSAMENTO(String FUNC_TAG, String FUNC_CALL, String directory, String userCode, String mainCpp, String header, String kernel, String mainCppOut, String headerOut, String kernelOut, String RETURN_TYPE_REGEX) {
        this.FUNC_TAG = FUNC_TAG;
        this.FUNC_CALL = FUNC_CALL;
        this.RETURN_TYPE_REGEX = RETURN_TYPE_REGEX;
        this.dir = directory;
        this.userCode = userCode;
        this.mainCpp = mainCpp;
        this.header = header;
        this.kernel = kernel;
        this.mainCppOut = mainCppOut;
        this.headerOut = headerOut;
        this.kernelOut = kernelOut;
    }

    public void setFile(String file){
        this.userCode = file;
    }
}
