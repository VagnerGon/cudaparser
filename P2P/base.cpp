#include "Subtraction.h"
#include <opencv2//core//core.hpp>
#include <opencv2//highgui//highgui.hpp>

#P2P_FUNC
static unsigned char subtraction(char in, data param, int i) {

	int result = in-param.imageSubtration[i] - param.limiar;
	result = result > 0 ? result : result * -1;
	return result;
}

#STRUCT
typedef struct {
	char* imageSubtration;
	int limiar;
} data;

void mostraResultado(IplImage* imagemOriginal, unsigned char* imagemProcessada){
	
	IplImage *imagemFinal;
	imagemFinal = cvCreateImageHeader(cvSize(imagemOriginal->width,imagemOriginal->height), IPL_DEPTH_8U, 1);
	cvSetData(imagemFinal,imagemProcessada,imagemOriginal->widthStep);

	cvShowImage("Original", imagemOriginal);
	cvShowImage("Polarizada", imagemFinal);

	cvWaitKey(100000);
}

int main() {
	
	IplImage *imagem = cvLoadImage("in.jpg", CV_LOAD_IMAGE_GRAYSCALE);
	auto imagebytes = imagem->imageData;
	int tamanhoImagem = imagem->imageSize;

	data dados;

	dados.imageSubtration = cvLoadImage("sub2.jpg", CV_LOAD_IMAGE_GRAYSCALE)->imageData;
	dados.limiar = 40;
	
	unsigned char* imagemResultante;	
	imagemResultante = static_cast<unsigned char*>(malloc(sizeof(unsigned char)*imagem->imageSize));
	for (int i = 0; i < tamanhoImagem; i++) {
		dados.indice = i;
		imagemResultante[i] = subtraction(imagebytes[i], dados);
	}

	mostraResultado(imagem, imagemResultante);
}
